from django.contrib.auth.models import User
from django.db import models


class GameModel(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    ip = models.GenericIPAddressField()
    port = models.IntegerField()
    admins = models.ManyToManyField(User, related_name='admin_of')
    token = models.CharField(max_length=50)
    created = models.DateField(auto_now_add=True)

    class Meta:
        ordering = ['created']
