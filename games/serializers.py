from abc import ABC
from rest_framework import serializers

from .models import GameModel


class TokenField(serializers.ReadOnlyField, ABC):
    def get_attribute(self, obj):
        if self.context.get('request').user in obj.admins.all():
            return super(TokenField, self).get_attribute(obj)
        return None


class GameSerializer(serializers.ModelSerializer):
    token = TokenField()

    class Meta:
        model = GameModel
        fields = ['id', 'name', 'ip', 'port', 'admins', 'token', 'created']
