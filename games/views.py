from rest_framework import viewsets, permissions

import string
import random

from .models import GameModel
from .serializers import GameSerializer

letters_and_numbers = string.ascii_letters + string.digits

class IsAdminOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user in obj.admins.all()


class GameViewSet(viewsets.ModelViewSet):
    queryset = GameModel.objects.all()
    serializer_class = GameSerializer
    permission_classes = [IsAdminOrReadOnly]

    def perform_create(self, serializer):
        token = ''.join(random.choices(letters_and_numbers, k=50))
        serializer.save(token=token)
